var vfs = require("vinyl-fs"),
	log = require("fancy-log"),
	copy = require("./copy");

module.exports = generate;

function generate(options) {
	log("Generating", options.AppName, "in ", options.directory);
	console.log(" ");

	return vfs
		.src([__dirname + "/app_template/**/*"], { dot: true })
		.pipe(copy(options))
		.pipe(vfs.dest(options.directory));
}
