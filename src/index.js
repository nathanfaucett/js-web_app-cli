var path = require("path"),
	asyncDone = require("@nathanfaucett/async_done"),
	underscore = require("@nathanfaucett/underscore"),
	generate = require("./generate"),
	install = require("./install");

module.exports = generateWebApp;

function generateWebApp(options, callback) {
	options = options || {};
	options.AppName = options.AppName || "WebApp";
	options.appName = options.appName || underscore(options.AppName);
	options.directory = path.join(
		process.cwd(),
		options.directory || options.appName
	);

	asyncDone(
		function asyncGenerate() {
			return generate(options);
		},
		function onGenerate(error) {
			if (error) {
				callback(error);
			} else if (!!options.install) {
				install(options, callback);
			} else {
				callback();
			}
		}
	);
}
