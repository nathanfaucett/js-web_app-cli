import { Router } from "@nathanfaucett/web_app";
import Page from "./components/layout/Page";
import PageNoHeader from "./components/layout/PageNoHeader";
import SignIn from "./components/SignIn";
import Home from "./components/Home";
import NotFound from "./components/NotFound";
import app from "./app";
import autoSignIn from "./middleware/autoSignIn";

const router = app.plugin(Router);

router.route("sign_in", "/sign_in", PageNoHeader(SignIn));

// all routes after this the user must be signed in
// ===============================================================
router.use(autoSignIn);

router.route("index", "/", Page(Home));

router.use(router.notFound(Page(NotFound)));
