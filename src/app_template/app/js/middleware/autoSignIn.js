import app from "../app";
import user from "../stores/user";

const autoSignIn = (ctx, next) => {
	user.autoSignIn(error => {
		if (error) {
			app.page.go("/sign_in");
		} else {
			next();
		}
	});
};

export default autoSignIn;
