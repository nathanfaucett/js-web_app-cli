import React from "react";
import { FormattedMessage } from "react-intl";

class Home extends React.Component {
	render() {
		return (
			<h1>
				<FormattedMessage id="app.name" />
			</h1>
		);
	}
}

export default Home;
