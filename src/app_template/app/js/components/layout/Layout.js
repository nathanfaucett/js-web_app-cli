import React from "react";
import Header from "./Header";
import Footer from "./Footer";

class Layout extends React.Component {
	render() {
		let className = "Layout";

		if (this.props.className) {
			className += " " + this.props.className;
		}

		return (
			<div className={className}>
				<Header />
				{this.props.children}
				<Footer />
			</div>
		);
	}
}

export default Layout;
