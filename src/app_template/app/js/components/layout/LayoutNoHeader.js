import React from "react";
import Footer from "./Footer";

class LayoutNoHeader extends React.Component {
    render() {
        let className = "LayoutNoHeader";

        if (this.props.className) {
            className += " " + this.props.className;
        }

        return (
            <div className={className}>
                {this.props.children}
                <Footer />
            </div>
        );
    }
}

export default LayoutNoHeader;
