import React from "react";
import LayoutNoHeader from "./LayoutNoHeader";

const PageNoHeader = Component => {
	class PageNoHeader extends React.Component {
		render() {
			return (
				<LayoutNoHeader
					className={Component.displayName || Component.name || ""}
				>
					<Component />
				</LayoutNoHeader>
			);
		}
	}
	return PageNoHeader;
};

export default PageNoHeader;
