import React from "react";
import Layout from "./Layout";

const Page = Component => {
	class Page extends React.Component {
		render() {
			return (
				<Layout
					className={Component.displayName || Component.name || ""}
				>
					<Component />
				</Layout>
			);
		}
	}
	return Page;
};

export default Page;
