import React from "react";
import { injectIntl } from "react-intl";
import connect from "@nathanfaucett/state-immutable-react";
import user from "../stores/user";
import app from "../app";

const signInForm = app.createStore("signInForm", {
    username: "",
    password: ""
});

class SignIn extends React.Component {
    constructor(props) {
        super(props);

        this.onSubmit = () => {
            const { username, password } = this.props.signInForm;

            user.signInWithToken(username + password + "token", () => {
                app.page.go("/");
                signInForm.setState({ username: "", password: "" });
            });
        };
        this.onUsernameChange = e => {
            signInForm.setState({ username: e.target.value });
        };
        this.onPasswordChange = e => {
            signInForm.setState({ password: e.target.value });
        };
    }

    render() {
        return (
            <form onSubmit={this.onSubmit}>
                <input
                    type="text"
                    placeholder={this.props.intl.formatMessage({
                        id: "sign_in.username"
                    })}
                    value={this.props.signInForm.username}
                    onChange={this.onUsernameChange}
                />
                <br />
                <input
                    type="password"
                    placeholder={this.props.intl.formatMessage({
                        id: "sign_in.password"
                    })}
                    value={this.props.signInForm.password}
                    onChange={this.onPasswordChange}
                />
                <br />
                <input
                    type="submit"
                    value={this.props.intl.formatMessage({
                        id: "sign_in.sign_in"
                    })}
                />
            </form>
        );
    }
}

export default connect([signInForm])(injectIntl(SignIn));
