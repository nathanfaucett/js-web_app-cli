import React from "react";
import { IntlProvider } from "react-intl";
import connect from "@nathanfaucett/state-immutable-react";
import { Router, Locales } from "@nathanfaucett/web_app";
import app from "../app";

const routerPlugin = app.plugin(Router),
    routerStore = routerPlugin.store(),
    localeStore = app.plugin(Locales).store(),
    { Provider, Consumer } = React.createContext({
        ctx: routerStore
            .state()
            .get("ctx")
            .toJS(),
        locale: localeStore.state().get("locale")
    });

class Root extends React.Component {
    render() {
        const { router, locales } = this.props,
            { ctx, state } = router,
            Component = routerPlugin.data(state),
            locale = locales.locale,
            messages = locales.locales[locale];

        return (
            <div className="Root">
                {messages ? (
                    <IntlProvider locale={locale} messages={messages}>
                        <Provider value={{ ctx: ctx, locale: locale }}>
                            {Component ? (
                                <Component />
                            ) : (
                                <p>
                                    No Component for State {state}, this should
                                    not happen, check you routes to see if you
                                    used a React Component
                                </p>
                            )}
                        </Provider>
                    </IntlProvider>
                ) : (
                    <p>Loading Locales...</p>
                )}
            </div>
        );
    }
}

export { Consumer as RootConsumer };

export default connect([routerStore, localeStore])(Root);
