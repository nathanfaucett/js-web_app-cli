module.exports = options => ({
	appUrl: "http://localhost:" + options.port,
	apiUrl: "http://localhost:" + options.apiPort
});
