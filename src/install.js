var childProcess = require("child_process"),
	log = require("fancy-log");

module.exports = install;

function install(options) {
	console.log(" ");
	log("npm install");

	childProcess.spawn("npm", ["install"], {
		cwd: options.directory,
		stdio: "inherit"
	});
}
