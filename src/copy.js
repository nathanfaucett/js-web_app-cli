var path = require("path"),
    through = require("through2"),
    File = require("vinyl"),
    log = require("fancy-log"),
    template = require("@nathanfaucett/template");

module.exports = copy;

function copy(options) {
    return through.obj(
        function(file, enc, callback) {
            var extname = path.extname(file.path),
                fullPath = file.path,
                contents = file.contents,
                relativePath;

            if (extname === ".ejs") {
                fullPath = path.join(
                    path.dirname(file.path),
                    path.basename(file.path, extname)
                );
                contents = template(file.contents.toString())(options);
                contents = new Buffer(contents);
            }

            relativePath = path.relative(file.base, fullPath);
            log(relativePath);

            this.push(
                new File({
                    base: options.directory,
                    path: path.join(options.directory, relativePath),
                    contents: contents
                })
            );

            callback();
        },
        function(callback) {
            callback();
        }
    );
}
