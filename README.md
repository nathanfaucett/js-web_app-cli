# js-web_app-cli

cli for bootstrapping web app projects

### Install

```bash
$ npm i @nathanfaucett/web_app-cli -g
```

### Usage

```bash
// -n is the package.json name, defaults to lowercase and underscored AppName
// -d is the directory name, defaults to -name
// -i installs npm packages
$ web_app new AppName -n app_name -d ./directory_name -i
```
