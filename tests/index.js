var tape = require("tape"),
	childProcess = require("child_process"),
	fs = require("fs"),
	fileUtils = require("@nathanfaucett/file_utils");

tape("default", assert => {
	if (fs.existsSync("./tests/web_app_test")) {
		fileUtils.removeSync("./tests/web_app_test");
	}

	try {
		childProcess.spawnSync("../bin/web_app", ["new", "WebAppTest", "-i"], {
			cwd: __dirname,
			stdio: "inherit"
		});
		childProcess.spawnSync("gulp", ["build"], {
			cwd: __dirname + "/web_app_test",
			stdio: "inherit"
		});
	} catch (error) {
		assert.end(error);
	}

	assert.end();
});
